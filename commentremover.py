#!/usr/bin/env python3

"""comment.py: It removes comments from "exe fmpolicy" object dump"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os
import sys


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal and displaying welcome message
os.system('cls' if os.name == 'nt' else 'clear')
print('\n\n\n\n\nWelcome!\nThis is FortiManager Comment remover v1.0\n')


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .txt files to list "cfgfiles"
for file in dirs:
    if ".txt" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
elif cfgindex == 1:
    print('\nOnly one file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
else:
    print('\nNo *.txt, *.conf or *.log files found. Exit!')
    sys.exit()
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()

filename = "./new_" + cfgfile
newfile = open(filename,'w+')

flag = 1
for line in cfg:
    if line.startswith("set comment"):
        line = "delete comment\n"
        newfile.write(line)
    if not line.startswith("set") and not line.startswith("delete"):
        newfile.write(line)
newfile.close()